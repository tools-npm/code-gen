
_czmbConfig = {
	img_path: "",
	// img_path: "campanhas/2018//",
	bannerType: 1,
	css: '#czmb-b-bg{position:absolute;z-index:1;width:730px;height:50px;left:0;bottom:0}#czmb-b-logo{position:absolute;z-index:2;width:101px;height:77px;left:0;bottom:0}#czmb-b-t1{position:absolute;z-index:3;width:222px;height:23px;left:158px;bottom:12px}#czmb-b-t2{position:absolute;z-index:4;width:240px;height:22px;left:430px;bottom:13px}#czmb-b-btn{position:absolute;z-index:5;width:425px;height:23px;left:206px;bottom:12px}#czmb-t-c3{position:absolute;z-index:10;width:200px;height:600px;left:400px;bottom:0}#czmb-t-c4{position:absolute;z-index:11;width:200px;height:600px;left:600px;bottom:0}#czmb-t-c5{position:absolute;z-index:12;width:200px;height:600px;left:800px;bottom:0}#czmb-t-bg{position:absolute;z-index:1;width:1000px;height:600px;left:0;bottom:0}#czmb-t-iframe-site{position:absolute;z-index:20;width:1000px;height:600px;left:0;bottom:0}#czmb-t-low{position:absolute;z-index:2;width:1000px;height:130px;left:0;bottom:0}#czmb-t-btn{position:absolute;z-index:3;width:159px;height:46px;left:421px;bottom:43px}#czmb-t-logo{position:absolute;z-index:4;width:154px;height:115px;left:423px;bottom:485px}#czmb-t-selo{position:absolute;z-index:5;width:213px;height:244px;left:143px;bottom:188px}#czmb-t-bg-video{position:absolute;z-index:6;width:452px;height:258px;left:506px;bottom:176px}#czmb-t-video{position:absolute;z-index:7;width:445px;height:250px;left:510px;bottom:180px}#czmb-t-c1{position:absolute;z-index:8;width:200px;height:600px;left:0;bottom:0}#czmb-t-c2{position:absolute;z-index:9;width:200px;height:600px;left:200px;bottom:0}#czmb-b{overflow:visible;position:fixed;left:50%;bottom:0;margin-left:-365px;width:730px!important;height:50px!important;font-size:35px;color:#fff;text-shadow:2px 2px 10px #004a69;cursor:pointer;line-height:50px;text-align:center;z-index:2147483645!important}#czmb-t{position:fixed;margin-left:-500px;left:50%;width:1000px;height:600px;cursor:pointer;bottom:0;background-color:#fff;z-index:2147483646;overflow:hidden;background:0 0}#czmb-t-close{position:fixed;left:50%;margin-left:475px;bottom:-100px;z-index:2147483646!important;width:50px;height:50px}#czmb-expansion-load{position:absolute;left:0;bottom:0;height:3px;background-color:#f7c174;z-index:90000;width:0%}',
	bot_images: ['bg'],
	top_images: ["bg"],
	videoId : "ZOlXkj-RO-M"
};

_czmbWide = new function() {
	this.anim;
	this.player;
	this.banner;
	this.cztk;
	this.click_url;
	this.clickThrottle;

	this.start = function() {
		if(typeof COMSCORE === "undefined"){
			_czmbWide.getComscore();
		}
		
		czQuery("head").append("<style type='text/css'>" + _czmbConfig.css + "</style>");

		this.anim = new TimelineMax();
		this.clickThrottle = false;
		this.cztk = _czmb.getCpnI(_czmbConfig.bannerType, 'tid');
		this.click_url = _czmb.getCpnI(1, "ucl").replace("[timestamp]", _czmb.gtt());
		this.banner = _czmbWide.createBanner();

		var app =
			"<div id='czmb-b'>" +
			"<div id='czmb-expansion-load'></div>" +
			"<div id='czmb-t'><div id='czmb-t-video'></div>" +
			"</div>";
		this.banner.append(app);
		this.banner.append('<img id="czmb-t-close" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE2LjAuMywgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8Y2lyY2xlIGZpbGw9IiMzQjNCM0IiIGN4PSIyNTYiIGN5PSIyNTYiIHI9IjI0Mi4yNiIvPgo8cGF0aCBmaWxsPSIjQzlDOUM5IiBkPSJNMjU2LjAwMSw2My43NGMxMDYuMTg0LDAsMTkyLjI2LDg2LjA3OCwxOTIuMjYsMTkyLjI2YzAsMTA2LjE4NC04Ni4wNzYsMTkyLjI2LTE5Mi4yNiwxOTIuMjYKCWMtMTA2LjE4MiwwLTE5Mi4yNi04Ni4wNzYtMTkyLjI2LTE5Mi4yNkM2My43NDEsMTQ5LjgxOCwxNDkuODE5LDYzLjc0LDI1Ni4wMDEsNjMuNzQgTTI1Ni4wMDEsNDMuNzQKCWMtMjguNjQ2LDAtNTYuNDQ2LDUuNjE1LTgyLjYyNywxNi42ODhjLTI1LjI3OCwxMC42OTEtNDcuOTc2LDI1Ljk5My02Ny40NjMsNDUuNDhjLTE5LjQ4NywxOS40ODctMzQuNzg5LDQyLjE4NS00NS40ODEsNjcuNDYzCglDNDkuMzU2LDE5OS41NTUsNDMuNzQxLDIyNy4zNTQsNDMuNzQxLDI1NmMwLDI4LjY0Niw1LjYxNSw1Ni40NDcsMTYuNjg4LDgyLjYyOWMxMC42OTIsMjUuMjc3LDI1Ljk5NCw0Ny45NzUsNDUuNDgxLDY3LjQ2MwoJYzE5LjQ4NywxOS40ODYsNDIuMTg1LDM0Ljc4OSw2Ny40NjMsNDUuNDhjMjYuMTgxLDExLjA3Miw1My45ODEsMTYuNjg4LDgyLjYyNywxNi42ODhjMjguNjQ2LDAsNTYuNDQ2LTUuNjE1LDgyLjYyOC0xNi42ODgKCWMyNS4yNzctMTAuNjkxLDQ3Ljk3Ny0yNS45OTQsNjcuNDYzLTQ1LjQ4YzE5LjQ4Ny0xOS40ODgsMzQuNzg5LTQyLjE4Niw0NS40OC02Ny40NjNjMTEuMDcyLTI2LjE4MiwxNi42ODgtNTMuOTgyLDE2LjY4OC04Mi42MjkKCWMwLTI4LjY0Ni01LjYxNS01Ni40NDUtMTYuNjg4LTgyLjYyN2MtMTAuNjkxLTI1LjI3Ny0yNS45OTMtNDcuOTc2LTQ1LjQ4LTY3LjQ2M2MtMTkuNDg2LTE5LjQ4Ny00Mi4xODYtMzQuNzg5LTY3LjQ2My00NS40OAoJQzMxMi40NDYsNDkuMzU1LDI4NC42NDcsNDMuNzQsMjU2LjAwMSw0My43NEwyNTYuMDAxLDQzLjc0eiIvPgo8cG9seWdvbiBmaWxsPSIjQzlDOUM5IiBwb2ludHM9IjIwMy40MjQsMzQzLjYzOSAxNjguMzczLDMwOC41NzYgMjIwLjk0OSwyNTYgMTY4LjM3MywyMDMuNDI1IDIwMy40MjQsMTY4LjM2MSAyNTYsMjIwLjk1IAoJMzA4LjU3NywxNjguMzYxIDM0My42MjcsMjAzLjQyNSAyOTEuMDUxLDI1NiAzNDMuNjI3LDMwOC41NzYgMzA4LjU3NywzNDMuNjM5IDI1NiwyOTEuMDUxICIvPgo8L3N2Zz4=">');

		czQuery.each(_czmbConfig.bot_images, function(k, v) {
		  var source = 'bottom/czmb-b-' + v + '.png';
		  var id = 'czmb-b-' + v;
		  var divId = 'czmb-b';
		  _czmb.load_img(_czmbConfig.img_path+source,id,divId);
		});

		czQuery.each(_czmbConfig.top_images, function(k, v) {
		  var source = 'top/czmb-t-' + v + '.png';
		  var id = 'czmb-t-' + v;
		  var divId = 'czmb-t';
		  _czmb.load_img(_czmbConfig.img_path+source,id,divId);
		});

		// _czmb.load_img(_czmbConfig.img_path+'top/czmb-t-bg.jpg','czmb-t-bg','czmb-t');
		
		 /* Preparando Vídeo */
		_czmbWide.prepareVideo();

		setTimeout(function() {
			_czmb.pushTrack(_czmbWide.cztk, 'print');
			_czmbWide.comscorePrint();
			_czmbWide.banner.removeAttr("style");
			czQuery("#czmb-b").click(function() {
				_czmbWide.click();
				_czmbWide.banner.unbind("mouseleave");
				_czmbWide.banner.mouseenter();
			});
			czQuery("#czmb-t-close").click(function() {
				_czmbWide.close();
			});
			czQuery("#czmb-t").children().click(function(e) {
				_czmbWide.click(e);
			});
			_czmbWide.actions();
			_czmbWide.animations();
			_czmbWide.banner.show();
		}, 2000);
	};

	this.getComscore = function(a) {
		var b = document.createElement("script"),
			d = document.getElementsByTagName("head")[0],
			c = [];
		c.push("(function() {");
		c.push('var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";el.parentNode.insertBefore(s, el);');
		c.push("})();");
		setTimeout(function() {
				"function" === typeof a && a()
			},
			1E3);
		b.text = c.join("");
		d.appendChild(b)
	};
	this.comscorePlay = function() {
		"undefined" !== typeof COMSCORE ? COMSCORE.beacon({
			c1: 1,
			c2: "16041074",
			c5: "12"
		}) : _czmbWide.getComscore(_czmbWide.comscorePlay)
	};
	this.comscorePrint = function() {
		"undefined" !== typeof COMSCORE ? COMSCORE.beacon({
			c1: 1,
			c2: "16041074",
			c5: "12"
		}) : _czmbWide.getComscore(_czmbWide.comscorePrint)
	};

	this.close = function() {
		this.anim.pause(0.5);
		this.endVideo();

		czQuery("#czmb-t-close").animate({
			bottom: -100,
			autoAlpha: 0
		});
		czQuery("#czmb-b").animate({
			right: "110%"
		}, 500);

		_czmbWide.actions();
		_czmbWide.anim.pause(0);
		TweenMax.set("#czmb-t", {
			autoAlpha: 0
		});

		czQuery("#cz-overlay").fadeOut().remove();
	};

	this.click = function(e) {
		if(_czmbWide.clickThrottle === true) return false;
		_czmbWide.clickThrottle = true;
		_czmb.pushTrack(this.cztk, 'click');
		window.open(_czmbWide.click_url);
		_czmbWide.close();
		setTimeout(function() {
			_czmbWide.close();
			_czmbWide.clickThrottle = false;
		}, 800);
	};

	this.createBanner = function() {
		if (_czmbConfig.bannerType == 1) type = "czmb-wide";
		czQuery("body").append("<div id=" + type + " style='bottom:-500px'></div>");
		var banner = czQuery("#" + type);
		if (_czmb.getCpnI(1, 'upr') != "") {
			banner.append('<img src="' + _czmb.getCpnI(1, 'upr').replace('[timestamp]', _czmb.gtt()) + '" width="1" height="1" border="0" alt="" />');
		}
		banner.hide();
		return banner;
	}

	this.actions = function() {
		this.banner.unbind("mouseenter");
		this.banner.unbind("mouseleave");

		this.banner.mouseenter(function() {
			czQuery("#czmb-expansion-load").stop().animate({
				width: "100%"
			}, 800, function() {
				_czmbWide.banner.unbind("mouseenter");
				_czmbWide.banner.unbind("mouseleave");
				_czmbWide.playBanner();
				_czmb.pushTrack(_czmbWide.cztk, 'interaction');
				czQuery("#czmb-expansion-load").animate({
					width: "0%"
				}, 800);
				czQuery("body").append("<div id='cz-overlay'></div>");
				TweenMax.set("#cz-overlay", {
					autoAlpha: 0,
					position: "fixed",
					zIndex: 2147483640,
					width: "100%",
					height: "100%",
					backgroundColor: "black",
					top: 0,
					left: 0
				});
				czQuery("#cz-overlay").click(function() {
					_czmbWide.close();
				});
				TweenMax.to("#cz-overlay", 0.5, {
					autoAlpha: 0.5
				});
			});
		});

		this.banner.mouseleave(function() {
			czQuery("#czmb-expansion-load").stop().animate({
				width: "0%"
			}, 500);
		});
	};

	this.playBanner = function() {
		czQuery("#czmb-b").fadeIn();
		if (_czmb.getCpnI(1, 'uin') != "") {
			_czmbWide.banner.append('<img src="' + _czmb.getCpnI(1, 'uin').replace('[timestamp]', _czmb.gtt()) + '" width="1" height="1" border="0" alt="" />');
		}
		this.anim.play();
		czQuery("#czmb-t-close").animate({
			bottom: 575,
			opacity: 1
		});
	};

	this.animations = function() {
		var bottom = {
			"background": new TimelineMax(),
			"text": new TimelineMax()
		};

		bottom.background
			.addLabel("start")
			.from('#czmb-b-bg', 1, {alpha: 0, y: 100}, "start")
			.addCallback(function(){
				// bottom.text.play();
			}, "start+=.8");
		
		bottom.text.pause(0);
		bottom.text
			.addLabel("start")
			.from('#czmb-b-t1', .7, {autoAlpha: 0, rotationX: 90, x: -100, ease: Power3.easeOut}, 'start')
			.from('#czmb-b-t2', .7, {autoAlpha: 0, rotationX: 90, x: 100, ease: Power3.easeOut}, 'start+=.2')

			.to("#czmb-b-t1", .5, {autoAlpha: 0, rotationX: 0, x: -100, ease: Power3.easeIn}, "start+=2")
			.to("#czmb-b-t2", .5, {autoAlpha: 0, rotationX: 0, x: 100, ease: Power3.easeIn}, "start+=2.2")

			.from('#czmb-b-btn', .7, {autoAlpha: 0, rotationX: 90, y: 100, ease: Power3.easeOut}, 'start+=2.6')
			.to('#czmb-b-btn', .5, {autoAlpha: 0, rotationX: 90, y: 100, ease: Power3.easeIn}, 'start+=5.6')

			.repeat(-1)
	

		this.anim.pause(0);
		
		this.anim
			.addLabel('start')
				.from("#czmb-t", .25, {autoAlpha:0, height:0}, "start")
				.from("#czmb-t-bg", .5, {autoAlpha:0, y: 600}, "start")
				.from("#czmb-t-video", .7, {autoAlpha: 0, x: 400, ease: Power3.easeOut}, "start+=.5")
				.addCallback(function () {
					_czmbWide.player.playVideo();
					_czmbWide.checkAnotherEvents();
				}, "start+=.5")


		czQuery("#czmb-t-btn").mouseenter(function(){
			TweenMax.to(czQuery(this),0.6,{scale:1.1});
		}).mouseout(function(){
			TweenMax.to(czQuery(this),0.6,{scale:1});
		});

		
	};
	this.prepareVideo = function() {
		_czmbWide.player = new YT.Player("czmb-t-video", {
			height: '100%',
			width: '100%',
			videoId: _czmbConfig.videoId,
			playerVars: {
				rel: 0,
				controls: 1,
				showinfo: 0,
				html5: 1,
				color: "white"
			},
			events: {
				'onStateChange': function(event) {
					if (event.data == YT.PlayerState.PLAYING) {
						_czmbWide.video_play = Math.round(new Date().getTime());
					}
				}
			}
		});
	};
	this.endVideo = function() {
		var now = Math.round(new Date().getTime());
		if (_czmbWide.video_play > 0 && (now - _czmbWide.video_play) > 0) {
			var time = (now - _czmbWide.video_play) / 1000;
			time = time.toFixed(1);
			if (time > _czmbWide.video_total) {
				time = _czmbWide.video_total;
			}
			_czmb.pushVideoData(_czmb.getCpnI(1, 'bid'), 'video_time', _czmbWide.player.getCurrentTime());
		}
		_czmbWide.video_play = 0;
		_czmbWide.disableVideo();
	};
	this.disableVideo = function() {
		_czmbWide.player.seekTo(0);
		_czmbWide.player.pauseVideo();
	};

	this.checkAnotherEvents = function(){
		window._czmb.pushTrack(window._czmb.getCpnI(_czmbConfig.bannerType, 'tid'),'video_play')
        var bvd = window._czmb.getCpnI(_czmbConfig.bannerType, 'bvd');
        if ((bvd == "") || (bvd == 0) || (typeof bvd === 'undefined')) {
            bvd = 0;
        }

        var videoTime = _czmbWide.player.getDuration();
        var quartile = videoTime/4;
        var already = {
            'impression' : false,
            'first_quartile' : false,
            'second_quartile' : false,
            'third_quartile' : false,
            'completed' : false
        };
        var videoEventsInterval = setInterval(function(){
            if ((!already.impression) && (_czmbWide.player.getCurrentTime() >= bvd)) {
                already.impression = true;
                window._czmb.pushTrack(window._czmb.getCpnI(_czmbConfig.bannerType, 'tid'), 'video_impression');
            } else if((!already.first_quartile) && (_czmbWide.player.getCurrentTime() >= quartile)) {
                already.first_quartile = true;
                window._czmb.pushTrack(window._czmb.getCpnI(_czmbConfig.bannerType, 'tid'), 'video_first_quartile');
            } else if((!already.second_quartile) && (_czmbWide.player.getCurrentTime() >= quartile*2)) {
                already.second_quartile = true;
                window._czmb.pushTrack(window._czmb.getCpnI(_czmbConfig.bannerType, 'tid'), 'video_second_quartile');
            } else if((!already.third_quartile) && (_czmbWide.player.getCurrentTime() >= quartile*3)) {
                already.third_quartile = true;
                window._czmb.pushTrack(window._czmb.getCpnI(_czmbConfig.bannerType, 'tid'), 'video_third_quartile');
            } else if((!already.completed) && (_czmbWide.player.getCurrentTime() >= (quartile*4-2))) {
                clearInterval(videoEventsInterval);
                already.completed = true;
                window._czmb.pushTrack(window._czmb.getCpnI(_czmbConfig.bannerType, 'tid'), 'video_complete');
            } 
        }, 300);
    }

}

/* . . . Aguardando o YouTubeIframeAPI ser totalmente carregado . . . */
if(typeof(YT) == 'undefined'){
	var youtube = document.createElement('script');
	youtube.src = "https://www.youtube.com/iframe_api";

	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(youtube, firstScriptTag);
	function onYouTubeIframeAPIReady() {
		_czmbWide.start();
		czQuery.ajaxSetup({
			cache: false
		});
	}
} else {
	_czmbWide.start();
	czQuery.ajaxSetup({
		cache: false
	});
}




