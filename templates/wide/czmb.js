_czmbConfig = {
	img_path: "",
	bannerType: 1,
	css: "#czmb-b-bg{position:absolute;z-index:1;width:730px;height:50px;left:0;bottom:0}#czmb-t-bg{position:absolute;z-index:1;width:1000px;height:600px;left:0;bottom:0}#czmb-b{overflow:visible;position:fixed;left:50%;bottom:0;margin-left:-365px;width:730px!important;height:50px!important;font-size:35px;color:#fff;text-shadow:2px 2px 10px #004a69;cursor:pointer;line-height:50px;text-align:center;z-index:2147483645!important}#czmb-t{position:fixed;margin-left:-500px;left:50%;width:1000px;height:600px;cursor:pointer;bottom:0;background-color:#fff;z-index:2147483646;overflow:hidden;background:0 0}#czmb-t-close{position:fixed;left:50%;margin-left:475px;bottom:-100px;z-index:2147483646!important;width:50px;height:50px}#czmb-expansion-load{position:absolute;left:0;bottom:0;height:3px;background-color:#f7c174;z-index:90000;width:0%}",
	bot_images: ["bg"],
	top_images: ["bg"],
};

_czmbWide = new function() {

	this.anim;
	this.player;
	this.banner;
	this.cztk;
	this.click_url;
	this.clickThrottle;

	this.start = function() {		
		jQuery("head").append("<style type='text/css'>" + _czmbConfig.css + "</style>");

		this.anim = new TimelineMax();
		this.clickThrottle = false;
		this.cztk = _czmb.getCpnI(_czmbConfig.bannerType, 'tid');
		this.click_url = _czmb.getCpnI(1, "ucl").replace("[timestamp]", _czmb.gtt());
		this.banner = _czmbWide.createBanner();

		var app =
			"<div id='czmb-b'><div id='czmb-expansion-load'></div></div>" +
			"<div id='czmb-t'></div>";
		this.banner.append(app);
		this.banner.append('<img id="czmb-t-close" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE2LjAuMywgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8Y2lyY2xlIGZpbGw9IiMzQjNCM0IiIGN4PSIyNTYiIGN5PSIyNTYiIHI9IjI0Mi4yNiIvPgo8cGF0aCBmaWxsPSIjQzlDOUM5IiBkPSJNMjU2LjAwMSw2My43NGMxMDYuMTg0LDAsMTkyLjI2LDg2LjA3OCwxOTIuMjYsMTkyLjI2YzAsMTA2LjE4NC04Ni4wNzYsMTkyLjI2LTE5Mi4yNiwxOTIuMjYKCWMtMTA2LjE4MiwwLTE5Mi4yNi04Ni4wNzYtMTkyLjI2LTE5Mi4yNkM2My43NDEsMTQ5LjgxOCwxNDkuODE5LDYzLjc0LDI1Ni4wMDEsNjMuNzQgTTI1Ni4wMDEsNDMuNzQKCWMtMjguNjQ2LDAtNTYuNDQ2LDUuNjE1LTgyLjYyNywxNi42ODhjLTI1LjI3OCwxMC42OTEtNDcuOTc2LDI1Ljk5My02Ny40NjMsNDUuNDhjLTE5LjQ4NywxOS40ODctMzQuNzg5LDQyLjE4NS00NS40ODEsNjcuNDYzCglDNDkuMzU2LDE5OS41NTUsNDMuNzQxLDIyNy4zNTQsNDMuNzQxLDI1NmMwLDI4LjY0Niw1LjYxNSw1Ni40NDcsMTYuNjg4LDgyLjYyOWMxMC42OTIsMjUuMjc3LDI1Ljk5NCw0Ny45NzUsNDUuNDgxLDY3LjQ2MwoJYzE5LjQ4NywxOS40ODYsNDIuMTg1LDM0Ljc4OSw2Ny40NjMsNDUuNDhjMjYuMTgxLDExLjA3Miw1My45ODEsMTYuNjg4LDgyLjYyNywxNi42ODhjMjguNjQ2LDAsNTYuNDQ2LTUuNjE1LDgyLjYyOC0xNi42ODgKCWMyNS4yNzctMTAuNjkxLDQ3Ljk3Ny0yNS45OTQsNjcuNDYzLTQ1LjQ4YzE5LjQ4Ny0xOS40ODgsMzQuNzg5LTQyLjE4Niw0NS40OC02Ny40NjNjMTEuMDcyLTI2LjE4MiwxNi42ODgtNTMuOTgyLDE2LjY4OC04Mi42MjkKCWMwLTI4LjY0Ni01LjYxNS01Ni40NDUtMTYuNjg4LTgyLjYyN2MtMTAuNjkxLTI1LjI3Ny0yNS45OTMtNDcuOTc2LTQ1LjQ4LTY3LjQ2M2MtMTkuNDg2LTE5LjQ4Ny00Mi4xODYtMzQuNzg5LTY3LjQ2My00NS40OAoJQzMxMi40NDYsNDkuMzU1LDI4NC42NDcsNDMuNzQsMjU2LjAwMSw0My43NEwyNTYuMDAxLDQzLjc0eiIvPgo8cG9seWdvbiBmaWxsPSIjQzlDOUM5IiBwb2ludHM9IjIwMy40MjQsMzQzLjYzOSAxNjguMzczLDMwOC41NzYgMjIwLjk0OSwyNTYgMTY4LjM3MywyMDMuNDI1IDIwMy40MjQsMTY4LjM2MSAyNTYsMjIwLjk1IAoJMzA4LjU3NywxNjguMzYxIDM0My42MjcsMjAzLjQyNSAyOTEuMDUxLDI1NiAzNDMuNjI3LDMwOC41NzYgMzA4LjU3NywzNDMuNjM5IDI1NiwyOTEuMDUxICIvPgo8L3N2Zz4=">');

		jQuery.each(_czmbConfig.bot_images, function(k, v) {
		  var source = 'bottom/czmb-b-' + v + '.png';
		  var id = 'czmb-b-' + v;
		  var divId = 'czmb-b';
		  _czmb.load_img(_czmbConfig.img_path+source,id,divId);
		});

		jQuery.each(_czmbConfig.top_images, function(k, v) {
		  var source = 'top/czmb-t-' + v + '.png';
		  var id = 'czmb-t-' + v;
		  var divId = 'czmb-t';
		  _czmb.load_img(_czmbConfig.img_path+source,id,divId);
		});

		setTimeout(function() {
			_czmb.pushTrack(_czmbWide.cztk, 'print');
			_czmbWide.banner.removeAttr("style");
			jQuery("#czmb-b").click(function() {
				_czmbWide.click();
				_czmbWide.banner.unbind("mouseleave");
				_czmbWide.banner.mouseenter();
			});
			jQuery("#czmb-t-close").click(function() {
				_czmbWide.close();
			});
			jQuery("#czmb-t").children().click(function(e) {
				_czmbWide.click(e);
			});
			_czmbWide.actions();
			_czmbWide.animations();
			_czmbWide.banner.show();
		}, 2000);
	};

	this.close = function() {
		this.anim.pause(0.5);

		// _czmbWide.sendExpansionTimeEvent();
		jQuery("#czmb-t-close").animate({
			bottom: -100,
			autoAlpha: 0
		});
		jQuery("#czmb-b").animate({
			right: "110%"
		}, 500);

		_czmbWide.actions();
		_czmbWide.anim.pause(0);
		TweenMax.set("#czmb-t", {
			autoAlpha: 0
		});

		jQuery("#cz-overlay").fadeOut().remove();
	};

	this.click = function(e) {
		if(_czmbWide.clickThrottle === true) return false;
		_czmbWide.clickThrottle = true;
		_czmb.pushTrack(this.cztk, 'click');
		window.open(_czmbWide.click_url);
		_czmbWide.close();
		setTimeout(function() {
			_czmbWide.close();
			_czmbWide.clickThrottle = false;
		}, 800);
	};

	this.createBanner = function() {
		if (_czmbConfig.bannerType == 1) type = "czmb-wide";
		jQuery("body").append("<div id=" + type + " style='bottom:-500px'></div>");
		var banner = jQuery("#" + type);
		if (_czmb.getCpnI(1, 'upr') != "") {
			banner.append('<img src="' + _czmb.getCpnI(1, 'upr').replace('[timestamp]', _czmb.gtt()) + '" width="1" height="1" border="0" alt="" />');
		}
		banner.hide();
		return banner;
	}

	this.actions = function() {
		this.banner.unbind("mouseenter");
		this.banner.unbind("mouseleave");

		this.banner.mouseenter(function() {
			jQuery("#czmb-expansion-load").stop().animate({
				width: "100%"
			}, 800, function() {
				_czmbWide.banner.unbind("mouseenter");
				_czmbWide.banner.unbind("mouseleave");
				_czmbWide.playBanner();
				_czmb.pushTrack(_czmbWide.cztk, 'interaction');
				jQuery("#czmb-expansion-load").animate({
					width: "0%"
				}, 800);
				jQuery("body").append("<div id='cz-overlay'></div>");
				TweenMax.set("#cz-overlay", {
					autoAlpha: 0,
					position: "fixed",
					zIndex: 2147483640,
					width: "100%",
					height: "100%",
					backgroundColor: "black",
					top: 0,
					left: 0
				});
				jQuery("#cz-overlay").click(function() {
					_czmbWide.close();
				});
				TweenMax.to("#cz-overlay", 0.5, {
					autoAlpha: 0.5
				});
			});
		});

		this.banner.mouseleave(function() {
			jQuery("#czmb-expansion-load").stop().animate({
				width: "0%"
			}, 500);
		});
	};

	this.playBanner = function() {
		jQuery("#czmb-b").fadeIn();
		if (_czmb.getCpnI(1, 'uin') != "") {
			_czmbWide.banner.append('<img src="' + _czmb.getCpnI(1, 'uin').replace('[timestamp]', _czmb.gtt()) + '" width="1" height="1" border="0" alt="" />');
		}
		this.anim.play();
		jQuery("#czmb-t-close").animate({
			bottom: 575,
			opacity: 1
		});
	};

	this.animations = function() {
		var bottom = {
			"background": new TimelineMax(),
			"text": new TimelineMax()
		};
		
		bottom.background
			.from('#czmb-b-bg', .7, {autoAlpha: 0, y: 100}, "start")
			
			.addCallback(function(){
				// bottom.text.play();
			}, "start+=.5");

		bottom.text.pause(0);
		bottom.text.addLabel("start")
		.addLabel("start")
			.from('#czmb-b-t1', .7, {autoAlpha: 0, rotationX: 90, x: -100, ease: Power3.easeOut}, 'start')
			.from('#czmb-b-t2', .7, {autoAlpha: 0, rotationX: 90, x: 100, ease: Power3.easeOut}, 'start+=.2')

			.to("#czmb-b-t1", .5, {autoAlpha: 0, rotationX: 0, x: -100, ease: Power3.easeIn}, "start+=2")
			.to("#czmb-b-t2", .5, {autoAlpha: 0, rotationX: 0, x: 100, ease: Power3.easeIn}, "start+=2.2")

			.from('#czmb-b-btn', .7, {autoAlpha: 0, rotationX: 90, y: 100, ease: Power3.easeOut}, 'start+=2.6')
			.to('#czmb-b-btn', .5, {autoAlpha: 0, rotationX: 90, y: 100, ease: Power3.easeIn}, 'start+=5.6')
			.repeat(-1)

		//TOP	
 		this.anim.pause(0);
		this.anim.addLabel("start")
			.from("#czmb-t", .25, {autoAlpha:0, height:0}, "start")
			.from("#czmb-t-bg", .7, {autoAlpha: 0, y: 600, ease: Power3.easeOut}, "start")

			
		jQuery("#czmb-t-btn").mouseenter(function(){
			TweenMax.to(jQuery(this),0.6,{scale:1.1});
		}).mouseout(function(){
			TweenMax.to(jQuery(this),0.6,{scale:1});
		});		
	};

}
_czmbWide.start();
jQuery.ajaxSetup({
	cache: false
});
