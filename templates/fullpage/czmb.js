_czmbConfig = {
    path: "",//"campanhas/2018//",
    bannerType: 6,
    css: '#czmb-m-bg{position:absolute;z-index:1;width:640px;height:960px;left:0;bottom:0}#czmb-fpage-inner{overflow:hidden;position:fixed;left:50%;top:50%;margin-left:-320px;margin-top:-480px;width:640px!important;height:960px!important;z-index:2147483646!important;background-color:#fff}#czmb-fpage{position:fixed;left:0;top:0;width:100%!important;height:100%!important;z-index:2147483645!important;display:none;background-color:rgba(0,0,0,.7)}#czmb-close{position:fixed;right:0;top:0;z-index:2147483647!important;width:10%;height:10%;display:none}',
    images: ["bg"]
};

_czmbMobile = new function () {

    this.anim;

    this.banner;
    this.click_url;
    this.cztk;
    this.orig_viewport;
    this.started;

    this.start = function() {
        if(this.started == 1) {
            return 0;
        } else {
            this.started = 0;
        }
        jQuery("head").append("<style type='text/css'>" + _czmbConfig.css + "</style>");
        jQuery("body").append("<div id='czmb-fpage'><div id='czmb-fpage-inner'></div></div>");
        jQuery("body").append("<img id='czmb-close' src='data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE2LjAuMywgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8Y2lyY2xlIGZpbGw9IiMzQjNCM0IiIGN4PSIyNTYiIGN5PSIyNTYiIHI9IjI0Mi4yNiIvPgo8cGF0aCBmaWxsPSIjQzlDOUM5IiBkPSJNMjU2LjAwMSw2My43NGMxMDYuMTg0LDAsMTkyLjI2LDg2LjA3OCwxOTIuMjYsMTkyLjI2YzAsMTA2LjE4NC04Ni4wNzYsMTkyLjI2LTE5Mi4yNiwxOTIuMjYKCWMtMTA2LjE4MiwwLTE5Mi4yNi04Ni4wNzYtMTkyLjI2LTE5Mi4yNkM2My43NDEsMTQ5LjgxOCwxNDkuODE5LDYzLjc0LDI1Ni4wMDEsNjMuNzQgTTI1Ni4wMDEsNDMuNzQKCWMtMjguNjQ2LDAtNTYuNDQ2LDUuNjE1LTgyLjYyNywxNi42ODhjLTI1LjI3OCwxMC42OTEtNDcuOTc2LDI1Ljk5My02Ny40NjMsNDUuNDhjLTE5LjQ4NywxOS40ODctMzQuNzg5LDQyLjE4NS00NS40ODEsNjcuNDYzCglDNDkuMzU2LDE5OS41NTUsNDMuNzQxLDIyNy4zNTQsNDMuNzQxLDI1NmMwLDI4LjY0Niw1LjYxNSw1Ni40NDcsMTYuNjg4LDgyLjYyOWMxMC42OTIsMjUuMjc3LDI1Ljk5NCw0Ny45NzUsNDUuNDgxLDY3LjQ2MwoJYzE5LjQ4NywxOS40ODYsNDIuMTg1LDM0Ljc4OSw2Ny40NjMsNDUuNDhjMjYuMTgxLDExLjA3Miw1My45ODEsMTYuNjg4LDgyLjYyNywxNi42ODhjMjguNjQ2LDAsNTYuNDQ2LTUuNjE1LDgyLjYyOC0xNi42ODgKCWMyNS4yNzctMTAuNjkxLDQ3Ljk3Ny0yNS45OTQsNjcuNDYzLTQ1LjQ4YzE5LjQ4Ny0xOS40ODgsMzQuNzg5LTQyLjE4Niw0NS40OC02Ny40NjNjMTEuMDcyLTI2LjE4MiwxNi42ODgtNTMuOTgyLDE2LjY4OC04Mi42MjkKCWMwLTI4LjY0Ni01LjYxNS01Ni40NDUtMTYuNjg4LTgyLjYyN2MtMTAuNjkxLTI1LjI3Ny0yNS45OTMtNDcuOTc2LTQ1LjQ4LTY3LjQ2M2MtMTkuNDg2LTE5LjQ4Ny00Mi4xODYtMzQuNzg5LTY3LjQ2My00NS40OAoJQzMxMi40NDYsNDkuMzU1LDI4NC42NDcsNDMuNzQsMjU2LjAwMSw0My43NEwyNTYuMDAxLDQzLjc0eiIvPgo8cG9seWdvbiBmaWxsPSIjQzlDOUM5IiBwb2ludHM9IjIwMy40MjQsMzQzLjYzOSAxNjguMzczLDMwOC41NzYgMjIwLjk0OSwyNTYgMTY4LjM3MywyMDMuNDI1IDIwMy40MjQsMTY4LjM2MSAyNTYsMjIwLjk1IAoJMzA4LjU3NywxNjguMzYxIDM0My42MjcsMjAzLjQyNSAyOTEuMDUxLDI1NiAzNDMuNjI3LDMwOC41NzYgMzA4LjU3NywzNDMuNjM5IDI1NiwyOTEuMDUxICIvPgo8L3N2Zz4='>");

        this.anim = new TimelineMax();

        this.orig_viewport = '';
        this.cztk = _czmb.getCpnI(_czmbConfig.bannerType, 'tid');
        this.click_url = _czmb.getCpnI(_czmbConfig.bannerType, 'ucl').replace('[timestamp]', _czmb.gtt());
        this.banner = jQuery("#czmb-fpage-inner");

        jQuery.each(_czmbConfig.images,function(k,v){_czmb.load_img(_czmbConfig.path+'czmb-m-'+v+'.png','czmb-m-'+v,'czmb-fpage-inner');});

        if (_czmb.getCpnI(_czmbConfig.bannerType, 'upr') != ""){
            this.banner.append('<img src="' + _czmb.getCpnI(_czmbConfig.bannerType, 'upr').replace('[timestamp]', _czmb.gtt()) + '" width="1" height="1" border="0" alt="" />');   
        }

        var readyStateCheckInterval = setInterval(function() {
		    if (document.readyState == "complete") {
                clearInterval(readyStateCheckInterval);

                _czmb.pushTrack(_czmbMobile.cztk, 'print');
                _czmbMobile.banner.click(function() {
                    _czmbMobile.click();
                });
                jQuery("#czmb-close").click(function() {
                    _czmbMobile.close();
                });

                jQuery("#czmb-fpage").fadeIn();
                jQuery("#czmb-close").fadeIn();

                _czmbMobile.orig_viewport = jQuery('meta[name="viewport"]').attr("content");

                var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
                jQuery('meta[name="viewport"]').attr("content", 'width=640, user-scalable=no');
                var scale = width / 640;
                scale = scale * 0.8;


                jQuery('meta[name="viewport"]').attr("content", 'width=640, initial-scale=' + scale + ',maximum-scale=' + scale + ', user-scalable=no,minimal-ui');

                _czmbMobile.animations();
		    }
		}, 100);
    };

    this.animations = function(){
        this.anim
            .delay(0.5)
            .from("#czmb-m-bg", .7, {autoAlpha:0, ease:Back.easeOut})

    }

    this.close = function() {
        jQuery("#czmb-close").fadeOut();
        jQuery("#czmb-fpage").fadeOut(function(){
			jQuery('meta[name="viewport"]').attr("content",_czmbMobile.orig_viewport);
        });
		jQuery("#czmb-close").remove();

        setTimeout(function(){jQuery("#czmb-fpage").remove();}, 1000);
    }

    this.click = function() {
        _czmb.pushTrack(_czmbMobile.cztk, 'click');
        window.open(_czmbMobile.click_url);
        _czmbMobile.close();
    }
};

_czmbMobile.start();