#!/usr/bin/env node

const figlet = require("figlet");
const chalk = require("chalk");
const clear = require("clear");

const inquirer = require("./lib/inquirer");
const shell = require("./lib/shell");

clear();

console.log(chalk.yellow(
    figlet.textSync('Code-Gen:', 'Star Wars', {horizontalLayout: "center"}))
);
console.log(chalk.yellow(
    figlet.textSync('Generate code', 'Star Wars', {horizontalLayout: "full"}))
);


const run = async () => {
    try{
        const bannerInfo = await inquirer.askBannerInfo();
        console.log(bannerInfo);
        shell.createBanner(bannerInfo);

    }
    catch(err) {
        console.log(err);
    }
    
}

run();