const inquirer = require("inquirer");
const files = require("./files");
const argv = require("minimist")(process.argv.slice(2));

const typeAndNameForm = [
    {
        type: "input",
        name: "name",
        message: "Nome para o pasta do banner:",
        default: argv._[0] || files.getCurrentDirectoryBase(),
        validate: function (value) {
            if (value.length){
                return true;
            }
            else{
                return "Amigo, a pasta do banner precisa de um nome!"
            }
        }
    },
    {
        type: "list",
        name: "type",
        message: "Tipo de banner a produzir",
        choices: ["wide", "fullpage", "video_slider", "Engage", "billboard", "html_pronto_wide", "html_pronto_fullpage", "side"],
        default: "wide"
    }
];

const youtubeForm = [
    {
        type: "confirm",
        name: "hasYoutube",
        message: "Deseja incluir a YouTube Video API + trackers?"
    }
]

let finalAnswer = {};

module.exports = {

    askBannerInfo: () => {
        
        return inquirer.prompt(typeAndNameForm)
            .then(answer => {
                finalAnswer.name = answer.name;
                finalAnswer.type = answer.type;

                switch(answer.type){
                    case "wide":
                    case "fullpage":
                    case "Engage":
                    case "billboard":
                    case "html_pronto_wide":
                    case "html_pronto":
                        return inquirer.prompt(youtubeForm)
                            .then(answer => {
                                finalAnswer.hasYoutube = answer.hasYoutube;        
                                return finalAnswer;
                            });
                    default:
                        return finalAnswer;
                }

            });
    }
    
}