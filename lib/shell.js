const shell = require("shelljs");
const fs = require("fs");

const root = require("../conf").root;

module.exports = {
    createBanner: (bannerInfo) => {
        
        shell.mkdir(bannerInfo.name);
        let template = {};
        switch (bannerInfo.type) {
            case "wide":
                
                makeFolders(bannerInfo.name, ["bottom", "top"]);
                makeFiles(bannerInfo.name, ["czmb.css", "czmb.js", "index.html"]);
                copyFiles(root + "/templates/general", ["fundo.jpg", "favicon.ico"], bannerInfo.name);
                copyFiles(root + "/templates/wide", ["bottom/czmb-b-bg.png"], bannerInfo.name + "/bottom");
                copyFiles(root + "/templates/wide", ["top/czmb-t-bg.png"], bannerInfo.name + "/top");
                

                
                if (bannerInfo.hasYoutube) {
                    template = {
                        html: loadTemplate(root + "/templates/general/index.html"),
                        czmb_youtube_js: loadTemplate(root + "/templates/wide/czmb_youtube.js"),
                        czmb_youtube_css: loadTemplate(root + "/templates/wide/czmb_youtube.css")
                    };
                    createContent(bannerInfo.name + "/czmb.js", template.czmb_youtube_js);
                    createContent(bannerInfo.name + "/czmb.css", template.czmb_youtube_css);
                }
                else {
                    template = {
                        html: loadTemplate(root + "/templates/general/index.html"),
                        czmbjs: loadTemplate( root + "/templates/wide/czmb.js"),
                        czmbcss: loadTemplate( root + "/templates/wide/czmb.css"),
                    };
                    createContent(bannerInfo.name + "/czmb.js", template.czmbjs);
                    createContent(bannerInfo.name + "/czmb.css", template.czmbcss);
    
                }
                createContent(bannerInfo.name + "/index.html", template.html); 
                break;
    
            case "fullpage":
                copyFiles(root + "/templates/general", ["fundo.jpg", "favicon.ico"], bannerInfo.name);
                copyFiles(root + "/templates/fullpage", ["czmb-m-bg.png"], bannerInfo.name);
                makeFiles(bannerInfo.name, ["czmb.css", "czmb.js", "index.html"]);

                if (bannerInfo.hasYoutube) {
                    template = {
                        html: loadTemplate(root + "/templates/general/index.html"),
                        czmbjs: loadTemplate(root + "/templates/fullpage/czmb.js"),
                        czmbcss: loadTemplate(root + "/templates/fullpage/czmb.css")
                    };
                    createContent(bannerInfo.name + "/czmb.js", template.czmbjs);
                    createContent(bannerInfo.name + "/czmb.css", template.czmbcss);
                }
                else {
                    template = {
                        html: loadTemplate(root + "/templates/general/index.html"),
                        czmbjs: loadTemplate(root + "/templates/fullpage/czmb.js"),
                        czmbcss: loadTemplate(root + "/templates/fullpage/czmb.css")
                    };
                    createContent(bannerInfo.name + "/czmb.js", template.czmbjs);
                    createContent(bannerInfo.name + "/czmb.css", template.czmbcss);
                }
                createContent(bannerInfo.name + "/index.html", template.html); 
                break;
                
            default:
                break;
        }
    }
}


const makeFolders = (parent, folders) => {
    folders.forEach( folder => shell.mkdir( parent + "/" + folder));
}
const makeFiles = (parent, files) => {
    files.forEach(file => shell.touch(parent + "/" + file));
}

const copyFiles = (source, files, destination) => {
    files.forEach(file => shell.cp(source + "/" + file, destination));
}

const loadTemplate = (url) => {
    let data = fs.readFileSync(url, (err, data) => {
        if (err) {
            console.log(err);
            return
        }
        return data;
    });
    return data;
}
const createContent = (file, template) => {
    fs.appendFile(file, template, (err, data) => {
        if (err){console.log(err); return;}
        console.log(`nice ${file} incluido no banner!`);
    });
}